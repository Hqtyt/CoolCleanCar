using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class main : Node
{
	const int TileSize = 32;
	const int Columns = 4;
	const int Rows = 3;
	const float MudSoapRatio = 0.8f;

	private Sprite player;
	private Label scoreLabel;
	private Node2D tracksNode;
	private Sprite track;
	private Timer timer;
	private Button playAgain;

	private int score = 0;
	private RandomNumberGenerator rng;
	private List<Vector2> trackItems = new List<Vector2>();
	private List<Vector2> soapTracks = new List<Vector2>();
	private Vector2 playerPosition = new Vector2(1, 1);
	private int playerHealth = 3;


	public override void _Ready()
	{
		rng = new RandomNumberGenerator();
		rng.Randomize();

		player = (Sprite)GetNode("Player");
		playAgain = (Button)GetNode("GUI/PlayAgain");
		scoreLabel = (Label)GetNode("GUI/Score");
		timer = (Timer)scoreLabel.GetNode("Timer");
		tracksNode = (Node2D)GetNode("Tracks");
		track = (Sprite)tracksNode.GetNode("1_1");

		InitTracks();
	}

	private void _on_Timer_timeout()
	{
		score += 100;
		scoreLabel.Text = score.ToString();

		MoveMud();
		CheckItemsUnderPlayer();

		if (score % 2 == 0)
		{
			SpawnRandom();
		}

		if (playerHealth > 0)
		{
			timer.Start(1f / Mathf.Ceil((float)score / 1000f));
		}
	}

	private void MoveMud()
	{
		var unMovedTrackItems = new List<Vector2>(trackItems);
		trackItems.Clear();

		foreach (Vector2 itemPosition in unMovedTrackItems)
		{
			Vector2 newPosition = itemPosition - new Vector2(1, 0);
			var oldItem = GetTrackItem((int)itemPosition.x, (int)itemPosition.y);
			oldItem.Visible = false;

			if (newPosition.x <= 0)
			{
				continue;
			}

			var newItem = GetTrackItem((int)newPosition.x, (int)newPosition.y, oldItem.Name);

			newItem.Visible = true;
			trackItems.Add(newPosition);
		}
	}

	private void SpawnRandom()
	{
		Vector2 randSpawn = new Vector2(Columns, rng.RandiRange(1, Rows));
		var randomType = rng.Randf() < MudSoapRatio ? "Mud" : "Soap";
		Sprite randItem = GetTrackItem((int)randSpawn.x, (int)randSpawn.y, randomType);

		randItem.Visible = true;
		trackItems.Add(randSpawn);
	}

	private Sprite GetTrackItem(int column, int row, string itemType = "")
	{
		Sprite track = (Sprite)tracksNode.GetNode(String.Concat(column.ToString(), "_", row.ToString()));
		Sprite item = null;
		if (itemType != "")
		{
			item = (Sprite)track.GetNode(itemType);
		}
		else
		{
			foreach (Sprite trackItem in track.GetChildren())
			{
				if (trackItem.Visible)
				{
					item = trackItem;
					break;
				}
			}
		}

		return item;
	}

	private void CheckItemsUnderPlayer()
	{
		if (trackItems.Exists(itemPosition => itemPosition == playerPosition))
		{
			Sprite item = GetTrackItem((int)playerPosition.x, (int)playerPosition.y);

			playerHealth = item.Name == "Mud" ? playerHealth - 1 : playerHealth + 1;
			playerHealth = Mathf.Clamp(playerHealth, 0, 3);
			player.Frame = Mathf.Clamp(3 - playerHealth, 0, 2);

			if (playerHealth <= 0)
			{
				timer.Stop();
				playAgain.Disabled = false;
				playAgain.Visible = true;
			}
		}
	}

	private void InitTracks()
	{
		for (int column = 0; column < Columns; column++)
		{
			for (int row = 0; row < Rows; row++)
			{
				if (column == 0 && row == 0)
				{
					continue;
				}

				Sprite newTrack = (Sprite)track.Duplicate();
				newTrack.Name = String.Concat((column + 1).ToString(), "_", (row + 1).ToString());
				newTrack.Position = track.Position + new Vector2(column * TileSize, row * TileSize);

				tracksNode.AddChild(newTrack);
			}
		}
	}

	public override void _UnhandledInput(InputEvent @event)
	{
		if (timer.IsStopped())
		{
			return;
		}
		
		if (@event.IsActionPressed("up"))
		{
			playerPosition.y = Mathf.Max(1, playerPosition.y - 1);
		}
		else if (@event.IsActionPressed("down"))
		{
			playerPosition.y = Mathf.Min(Rows, playerPosition.y + 1);
		}
		else
		{
			return;
		}

		player.Position = new Vector2(16, 48) + (playerPosition - new Vector2(1, 1)) * TileSize;
		CheckItemsUnderPlayer();
	}

	private void PlayAgain()
	{
		foreach (Vector2 itemPosition in trackItems)
		{
			var item = GetTrackItem((int)itemPosition.x, (int)itemPosition.y);
			item.Visible = false;
		}

		trackItems.Clear();
		score = 0;
		playerHealth = 3;
		rng.Randomize();
		playAgain.Disabled = true;
		playAgain.Visible = false;
		player.Frame = 0;

		timer.Start(1);
	}
}

// https://metiorite.itch.io/
// https://skykone.itch.io/
// https://hqtyt.itch.io/